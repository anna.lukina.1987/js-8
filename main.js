const inpPrice = document.querySelector('.price');
const inpPriceWrap = document.querySelector('.price-wrap');
const priceContainer = document.querySelector('.price-container');

function handleError () {
    inpPrice.classList.add('error');
    const span = document.createElement('span');
    span.classList.add('error-text');
    span.innerHTML = 'Please enter correct price';

    inpPriceWrap.parentNode.insertBefore(span, inpPriceWrap.nextSibling);
}

function handleSuccess (fieldValue) {
    const priceControlsWrap = document.createElement('div');
    const btnClear = document.createElement('button');
    const span = document.createElement('span');
    btnClear.classList.add('clear-btn');
    priceControlsWrap.classList.add('price-controls-wrap');
    btnClear.innerHTML = `X`;
    span.innerHTML = `Текущая цена: ${fieldValue}`;

    priceControlsWrap.appendChild(span);
    priceControlsWrap.appendChild(btnClear);

    inpPriceWrap.parentNode.insertBefore(priceControlsWrap, inpPriceWrap);
    inpPrice.classList.add('selected');
}

function initInpFocusOutActions () {
    const errorText = document.querySelector('.error-text');
    const priceControlsWrap = document.querySelector('.price-controls-wrap');

    if ( errorText ) errorText.remove();
    if ( priceControlsWrap ) priceControlsWrap.remove();
    inpPrice.classList.remove('error');
}

inpPrice.addEventListener('focusout', function () {
    const fieldValue = inpPrice.value;
    const isLessOrEquealsZero = fieldValue <= 0;

    initInpFocusOutActions();

    if ( fieldValue === '' ) return;

    if ( isLessOrEquealsZero ) {
        handleError();
    } else {
        handleSuccess(fieldValue);
    }
})

priceContainer.addEventListener('click', function (event) {
    if ( event.target.classList.contains('clear-btn') ) {
        const priceControlsWrap = document.querySelector('.price-controls-wrap');

        inpPrice.value = '';
        inpPrice.classList.remove('selected');
        priceControlsWrap.remove();
    }
})


